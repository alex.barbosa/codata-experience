from django.db import models
from validacao.models import Validacao


class Arquivo(models.Model):
    arquivo_id = models.BigAutoField(primary_key=True)
    data_criacao = models.DateTimeField()
    data_atualizacao = models.DateTimeField()
    data_exclusao = models.DateTimeField(blank=True, null=True)
    arquivo = models.BinaryField(blank=True, null=True)
    validacao_fk = models.ForeignKey(Validacao, models.DO_NOTHING, db_column='validacao_fk', blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'arquivo'
