from django.db import models
from estagio_documento.models import EstagioDocumento
from proposta.models import Proposta
from usuario.models import User


class Validacao(models.Model):
    validacao_id = models.BigAutoField(primary_key=True)
    data_criacao = models.DateTimeField()
    data_atualizacao = models.DateTimeField()
    data_exclusao = models.DateTimeField(blank=True, null=True)
    status = models.BooleanField()
    proposta_fk = models.ForeignKey(
        Proposta, models.DO_NOTHING, db_column='proposta_fk', blank=True, null=True
    )
    estagio_id_estagiodocumento = models.ForeignKey(
        EstagioDocumento,
        models.DO_NOTHING,
        db_column='estagio_id_estagiodocumento',
        blank=True,
        null=True
    )
    usuario_fk = models.ForeignKey(User, models.DO_NOTHING, db_column='usuario_fk', blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'validacao'
