from django.db import models


class Funcao(models.Model):
    funcao_id = models.BigAutoField(primary_key=True)
    data_criacao = models.DateTimeField()
    data_atualizacao = models.DateTimeField()
    data_exclusao = models.DateTimeField(blank=True, null=True)
    funcao = models.CharField(max_length=50)

    class Meta:
        managed = False
        db_table = 'funcao'
        verbose_name = "Função"
        verbose_name_plural = "Funções"
        default_permissions = ()

    def __str__(self):
        return self.funcao