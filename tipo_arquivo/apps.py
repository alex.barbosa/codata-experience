from django.apps import AppConfig


class TipoArquivoConfig(AppConfig):
    name = 'tipo_arquivo'
