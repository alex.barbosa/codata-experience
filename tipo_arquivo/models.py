from django.db import models


class TipoArquivo(models.Model):
    tpa_id = models.SmallIntegerField(primary_key=True)
    tipo_arquivo_descricao = models.CharField(max_length=500, blank=True, null=True)
    data_criacao = models.DateTimeField()
    data_atualizacao = models.DateTimeField()
    data_exclusao = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'tipo_arquivo'
