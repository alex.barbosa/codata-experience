from django.db import models

from entidade_cnpj.models import EntidadeCnpj
from objeto.models import Objeto


class Edital(models.Model):
    edital_id = models.BigAutoField(primary_key=True)
    data_criacao = models.DateTimeField()
    data_atualizacao = models.DateTimeField()
    data_exclusao = models.DateTimeField(blank=True, null=True)
    nome_programa = models.CharField(max_length=150)
    tipo_chamada = models.BooleanField()
    interveniente_entidade = models.BooleanField()
    data_inicio = models.DateField(blank=True, null=True)
    data_fim = models.DateField(blank=True, null=True)
    descricao = models.TextField()
    objeto_id_objeto = models.ForeignKey(Objeto, models.DO_NOTHING, db_column='objeto_id_objeto', blank=True, null=True)
    entidade_cnpj_fk = models.SmallIntegerField(blank=True, null=True)
    entidade_cnpj_fk1 = models.ForeignKey(EntidadeCnpj, models.DO_NOTHING, db_column='entidade_cnpj_fk1', blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'edital'
