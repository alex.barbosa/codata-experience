from django.db import models
from django.utils import timezone
from django.core.mail import send_mail
from django.contrib.auth.models import AbstractBaseUser, PermissionsMixin, BaseUserManager

from endereco.models import Endereco
from entidade_cnpj.models import EntidadeCnpj
from perfil.models import Perfil


class UserManager(BaseUserManager):
    def _create_user(self, username, email, cpf, password, phone, is_superuser, is_staff, **extra_fields):
        now = timezone.now()
        if not username:
            raise ValueError('The given username must be set')
        email = self.normalize_email(email)
        user = self.model(
            username=username,
            email=email,
            cpf=cpf,
            is_active=True,
            phone=phone,
            is_superuser=is_superuser,
            is_staff=is_staff,
            joined_at=now,
            **extra_fields
        )
        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_user(self, username, email=None, cpf=None, password=None, phone=None, is_superuser=False, is_staff=False, **extra_fields):
        return self._create_user(username, email, cpf, password, phone, is_superuser, is_staff, **extra_fields)

    def create_superuser(self, username, email, cpf, password, phone, is_superuser=True, is_staff=True, **extra_fields):
        user = self._create_user(username, email, cpf, password, phone, is_superuser, is_staff, **extra_fields)
        user.is_active = True
        user.save(using=self._db)
        return user


class User(AbstractBaseUser, PermissionsMixin):
    username = models.CharField(max_length=50)
    cpf = models.CharField(max_length=11, unique=True)
    first_name = models.CharField(max_length=50)
    last_name = models.CharField(max_length=50)
    phone = models.DecimalField(max_digits=10, decimal_places=0)
    celular = models.DecimalField(max_digits=11, decimal_places=0, null=True)
    email = models.CharField(max_length=100)
    role = models.CharField(max_length=100, null=True)
    is_active = models.BooleanField()
    is_staff = models.BooleanField(default=False)
    id_address_fk = models.ForeignKey(Endereco, models.DO_NOTHING, db_column='id_address_fk',
                                      blank=True, null=True)
    id_profile_fk = models.ForeignKey(Perfil, models.DO_NOTHING, db_column='id_profile_fk',
                                      blank=True, null=True)
    id_entidade_cnpj_fk = models.ForeignKey(EntidadeCnpj, models.DO_NOTHING,
                                            db_column='id_entidade_cnpj_fk', blank=True, null=True)
    social_name = models.CharField(max_length=150, blank=True, null=True)
    party = models.CharField(max_length=150, null=True)
    joined_at = models.DateField()
    nationality = models.CharField(max_length=150, blank=True, null=True)
    party_logo = models.BinaryField(blank=True, null=True)
    birth_date = models.DateField(null=True)
    institutional_email = models.CharField(max_length=150, null=True)

    USERNAME_FIELD = 'cpf'
    REQUIRED_FIELDS = ['username', 'first_name', 'last_name', 'email', 'phone']

    objects = UserManager()

    class Meta:
        verbose_name = 'user'
        verbose_name_plural = 'users'

    def get_full_name(self):
        full_name = '%s %s' % (self.first_name, self.last_name)
        return full_name.strip()

    def get_short_name(self):
        return self.first_name

    def email_user(self, subject, message, from_email=None):
        send_mail(subject, message, from_email, [self.email])
