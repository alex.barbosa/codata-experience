from django.db import models

from perfil.models import Perfil


class EntidadeCnpj(models.Model):
    entidade_cnpj_id = models.BigAutoField(primary_key=True)
    data_criacao = models.DateTimeField()
    data_atualizacao = models.DateTimeField()
    data_exclusao = models.DateTimeField(blank=True, null=True)
    cnpj = models.DecimalField(max_digits=14, decimal_places=0)
    razao_social = models.CharField(max_length=150)
    nome_fantasia = models.CharField(max_length=150, blank=True, null=True)
    telefone = models.DecimalField(max_digits=10, decimal_places=0)
    email = models.CharField(max_length=150)
    unidade_orcamentaria = models.CharField(max_length=5, blank=True, null=True)
    brazao = models.BinaryField(blank=True, null=True)
    perfil_id_perfil = models.OneToOneField(Perfil, models.DO_NOTHING, db_column='perfil_id_perfil', blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'entidade_cnpj'

