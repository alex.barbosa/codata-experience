from django.apps import AppConfig


class EntidadeCnpjConfig(AppConfig):
    name = 'entidade_cnpj'
