from django.db import models

from municipio.models import Municipio


class Endereco(models.Model):
    endereco_id = models.BigAutoField(primary_key=True)
    data_criacao = models.DateTimeField()
    data_atualizacao = models.DateTimeField()
    data_exclusao = models.DateTimeField(blank=True, null=True)
    cep = models.DecimalField(max_digits=8, decimal_places=0)
    complemento = models.CharField(max_length=150, blank=True, null=True)
    numero = models.DecimalField(max_digits=10, decimal_places=0)
    logradouro = models.CharField(max_length=150)
    bairro = models.CharField(max_length=150)
    municipio_id_municipio = models.ForeignKey(Municipio, models.DO_NOTHING, db_column='municipio_id_municipio', blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'endereco'
