from django.db import models

from uf.models import Uf


class Municipio(models.Model):
    municipio_id = models.BigAutoField(primary_key=True)
    data_criacao = models.DateTimeField()
    data_atualizacao = models.DateTimeField()
    data_exclusao = models.DateTimeField(blank=True, null=True)
    municipio = models.CharField(max_length=30)
    uf_id_uf = models.ForeignKey(Uf, models.DO_NOTHING, db_column='uf_id_uf', blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'municipio'
        verbose_name = "Municipio"
        verbose_name_plural = "Municipios"
        default_permissions = ()

    def __str__(self):
        return self.municipio