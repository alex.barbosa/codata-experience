from django.db import models


class EstagioDocumento(models.Model):
    estagio_id = models.BigAutoField(primary_key=True)
    data_criacao = models.DateTimeField()
    data_atualizacao = models.DateTimeField()
    data_exclusao = models.DateTimeField(blank=True, null=True)
    estagio = models.CharField(max_length=2)
    ativo = models.BooleanField()

    class Meta:
        managed = False
        db_table = 'estagio_documento'
