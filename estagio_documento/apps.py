from django.apps import AppConfig


class EstagioDocumentoConfig(AppConfig):
    name = 'estagio_documento'
