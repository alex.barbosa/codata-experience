from django.db import models
from proposta.models import Proposta


class PlanoDeTrabalho(models.Model):
    plano_de_trabalho_id = models.BigAutoField(primary_key=True)
    data_criacao = models.DateTimeField()
    data_atualizacao = models.DateTimeField()
    data_exclusao = models.DateTimeField(blank=True, null=True)
    proposta_fk = models.OneToOneField(Proposta, models.DO_NOTHING, db_column='proposta_fk', blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'plano_de_trabalho'
