from django.apps import AppConfig


class PlanoDeTrabalhoConfig(AppConfig):
    name = 'plano_de_trabalho'
