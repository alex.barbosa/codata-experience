from django.apps import AppConfig


class TipoArquivoEditalConfig(AppConfig):
    name = 'tipo_arquivo_edital'
