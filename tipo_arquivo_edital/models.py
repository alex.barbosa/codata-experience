from django.db import models
from edital.models import Edital
from estagio_documento.models import EstagioDocumento
from tipo_arquivo.models import TipoArquivo


class TipoArquivoEdital(models.Model):
    tipo_arquivo_fk = models.ForeignKey(TipoArquivo, models.DO_NOTHING, db_column='tipo_arquivo_fk', blank=True, null=True)
    edital_fk = models.ForeignKey(Edital, models.DO_NOTHING, db_column='edital_fk', blank=True, null=True)
    estagio_documento_fk = models.ForeignKey(EstagioDocumento, models.DO_NOTHING, db_column='estagio_documento_fk', blank=True, null=True)
    modelo_arquivo = models.BinaryField(blank=True, null=True)
    data_criacao = models.DateTimeField()
    data_atualizacao = models.DateTimeField()
    data_exclusao = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'tipo_arquivo_edital'
