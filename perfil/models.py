from django.db import models
from funcao.models import Funcao


class Perfil(models.Model):
    perfil_id = models.BigAutoField(primary_key=True)
    data_criacao = models.DateTimeField()
    data_atualizacao = models.DateTimeField()
    data_exclusao = models.DateTimeField(blank=True, null=True)
    perfil = models.CharField(max_length=100)
    funcao_id_funcao = models.OneToOneField(Funcao, models.DO_NOTHING, db_column='funcao_id_funcao', blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'perfil'
        verbose_name = "Perfil"
        verbose_name_plural = "Perfis"
        default_permissions = ()

    def __str__(self):
        return self.perfil
