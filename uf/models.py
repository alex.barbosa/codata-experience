from django.db import models


class Uf(models.Model):
    uf_id = models.BigAutoField(primary_key=True)
    uf = models.CharField(max_length=2)
    data_criacao = models.DateTimeField()
    data_atualizacao = models.DateTimeField()
    data_exclusao = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'uf'
