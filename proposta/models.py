from django.db import models
from edital.models import Edital
from entidade_cnpj.models import EntidadeCnpj


class Proposta(models.Model):
    proposta_id = models.BigAutoField(primary_key=True)
    data_criacao = models.DateTimeField()
    data_atualizacao = models.DateTimeField()
    data_exclusao = models.DateTimeField(blank=True, null=True)
    nome_do_programa = models.CharField(max_length=150)
    edital_fk = models.ForeignKey(Edital, models.DO_NOTHING, db_column='edital_fk', blank=True, null=True)
    planotrabalho_fk = models.SmallIntegerField(blank=True, null=True)
    entidade_cnpj = models.DecimalField(max_digits=14, decimal_places=0, blank=True, null=True)
    entidade_cnpj_fk = models.ForeignKey(EntidadeCnpj, models.DO_NOTHING, db_column='entidade_cnpj_fk', blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'proposta'
