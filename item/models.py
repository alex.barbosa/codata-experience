from django.db import models
from plano_de_trabalho.models import PlanoDeTrabalho


class Item(models.Model):
    item_id = models.BigAutoField(primary_key=True)
    data_criacao = models.DateTimeField()
    data_atualizacao = models.DateTimeField()
    data_exclusao = models.DateTimeField(blank=True, null=True)
    acao = models.DecimalField(max_digits=3, decimal_places=0)
    etapa = models.DecimalField(max_digits=3, decimal_places=0)
    especificacao = models.CharField(max_length=150)
    quantidade = models.DecimalField(max_digits=9, decimal_places=0)
    unidade = models.CharField(max_length=30)
    valor_unitario = models.FloatField(db_column='valor unitario')  # Field renamed to remove unsuitable characters.
    inicio_etapa = models.DateField()
    fim_etapa = models.DateField()
    valor_etapa = models.FloatField()
    valor_acao = models.FloatField()
    plano_de_trabalho_id_plano_de_trabalho = models.ForeignKey(PlanoDeTrabalho, models.DO_NOTHING, db_column='plano_de_trabalho_id_plano_de_trabalho', blank=True, null=True)
    categoria_despesa = models.CharField(max_length=255, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'item'
