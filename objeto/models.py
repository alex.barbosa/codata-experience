from django.db import models


class Objeto(models.Model):
    objeto_id = models.BigAutoField(primary_key=True)
    data_criacao = models.DateTimeField()
    data_atualizacao = models.DateTimeField()
    data_exclusao = models.DateTimeField(blank=True, null=True)
    objeto = models.CharField(max_length=15)

    class Meta:
        managed = False
        db_table = 'objeto'
        verbose_name = "Objeto"
        verbose_name_plural = "Objetos"
        default_permissions = ()

    def __str__(self):
        return self.objeto